package cz.cvut.vratidan;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

public class SourceActivity extends AppCompatActivity implements SourceFragment.SourceFragmentListener {

    private AddSourceDialogFragment dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_source);

        Toolbar toolbar = (Toolbar) findViewById(R.id.source_toolbar);

        toolbar.setTitle(getString(R.string.source_activity));

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.source_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.add_source_action){
            FragmentManager fm = getSupportFragmentManager();
            this.dialog = AddSourceDialogFragment.newInstance((SourceFragment) fm.findFragmentById(R.id.source_fragment));
            this.dialog.show(fm, getString(R.string.add_source_dialog_tag));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
