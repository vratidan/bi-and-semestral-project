package cz.cvut.vratidan;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import cz.cvut.vratidan.room.model.Source;
import cz.cvut.vratidan.room.viewmodel.SourceViewModel;

public class AddSourceDialogFragment extends DialogFragment {

    private SourceViewModel sourceViewModel;

    public EditText nameEdit;

    public EditText linkEdit;

    private View view;

    public SourceFragment sf;

    public AddSourceDialogFragment(){}

    public static AddSourceDialogFragment newInstance(SourceFragment sf) {

        Bundle args = new Bundle();

        AddSourceDialogFragment fragment = new AddSourceDialogFragment();
        fragment.setArguments(args);

        fragment.sf = sf;

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = requireActivity().getLayoutInflater();

        this.view = inflater.inflate(R.layout.add_source_dialog, null);

        this.nameEdit = (EditText) this.view.findViewById(R.id.edit_title);

        this.linkEdit = (EditText) this.view.findViewById(R.id.edit_link);

        builder.setView(this.view)
                .setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addSourceToDatabase(nameEdit.getText().toString(), linkEdit.getText().toString());
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddSourceDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    public void addSourceToDatabase(String name, String link){
        int maxId = this.sf.sourceViewModel.getMaxId();

        Source source = new Source();
        source.setId(maxId+1);
        source.setName(name);
        source.setLink(link);

        this.sf.sourceViewModel.insert(source);

    }

}
