package cz.cvut.vratidan;

import android.app.job.JobParameters;
import android.app.job.JobService;

import cz.cvut.vratidan.room.repository.FeedRepository;

public class DownloadJobService extends JobService {

    public FeedRepository feedRepository;
    public JobParameters jobParameters;

    @Override
    public void onCreate() {
        super.onCreate();
        feedRepository = FeedReaderApplication.get(getApplicationContext()).getFeedRepository();
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        this.jobParameters = jobParameters;

        this.feedRepository.downloadFeeds();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {

        return false;
    }


}
