package cz.cvut.vratidan.room.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import cz.cvut.vratidan.FeedReaderApplication;
import cz.cvut.vratidan.room.AppDatabase;
import cz.cvut.vratidan.room.Resource;
import cz.cvut.vratidan.room.model.Feed;
import cz.cvut.vratidan.room.repository.FeedRepository;

public class FeedViewModel extends AndroidViewModel {

    public LiveData<List<Feed>> feedData;

    public FeedRepository repository;

    private AppDatabase db;



    public FeedViewModel(@NonNull Application application) {
        super(application);

        this.repository = FeedReaderApplication.get(application).getFeedRepository();
        this.feedData = this.repository.getAllFeeds();
        this.repository.downloadStatus.setValue(Resource.SUCCESS_STATUS);
    }

    public void downloadFeeds(){
        this.repository.downloadFeeds();
    }

    public LiveData<List<Feed>> getFeeds(){
        return feedData;
    }

    public MutableLiveData<Integer> getDownloadStatus(){
        return this.repository.downloadStatus;
    }

    public Feed findById(int id){
        return repository.feedDao.findById(id);
    }

    public void retryClicked(){
        this.downloadFeeds();
    }


}
