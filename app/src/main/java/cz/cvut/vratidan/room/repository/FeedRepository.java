package cz.cvut.vratidan.room.repository;

import android.os.AsyncTask;
import android.view.MenuItem;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndEntry;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndFeed;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.SyndFeedInput;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.XmlReader;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.cvut.vratidan.room.Resource;
import cz.cvut.vratidan.room.dao.FeedDao;
import cz.cvut.vratidan.room.dao.SourceDao;
import cz.cvut.vratidan.room.model.Feed;
import cz.cvut.vratidan.room.model.Source;

public class FeedRepository {

    public FeedDao feedDao;

    public SourceDao sourceDao;

    private MenuItem menuItem;

    public LiveData<List<Feed>> feedData;

    public LiveData<List<Source>> sourceData;

    public List<Source> sources;

    public MutableLiveData<Integer> downloadStatus = new MutableLiveData<>();

    private DownloadTask downloadTask;


    public FeedRepository(FeedDao feedDao, SourceDao sourceDao){
        this.feedDao = feedDao;
        this.sourceDao = sourceDao;
        this.feedData = this.feedDao.getAll();
        this.sourceData = this.sourceDao.getAll();
        this.sourceData.observeForever(new Observer<List<Source>>() {
            @Override
            public void onChanged(List<Source> s) {
                sources = s;
            }
        });
    }

    public LiveData<List<Feed>> getAllFeeds(){
        return this.feedData;
    }

    public void downloadFeeds(){
        if(downloadTask == null || downloadTask.getStatus() == AsyncTask.Status.FINISHED)
            downloadTask = new DownloadTask();
        downloadTask.execute();
    }

    public List<Source> getSources(){
        return this.sources;
    }

    public LiveData<List<Source>> getAllSources(){
        return sourceData;
    }

    public Source findSourceById(int id){
        return this.sourceDao.findById(id);
    }

    public int getSourceMaxId(){
        return this.sourceDao.getMaxId();
    }

    public void insertSource(Source ...sources){
        this.sourceDao.insert(sources);
    }

    public void deleteSource(Source source){
        this.sourceDao.delete(source);
    }

    public class DownloadTask extends AsyncTask<Boolean, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Boolean ...isJob) {

            List<Source> sourceList = getSources();

            if(sourceList == null)
                return false;

            Source [] sources = sourceList.toArray(new Source[sourceList.size()]);

            int maxID = feedDao.getMaxId();
            URL feedUrl;
            List<Feed> createdFeeds = new ArrayList<>();
            boolean validData = true;


            for (Source source:
                    sources) {
                try{
                    feedUrl = new URL(source.getLink());

                    SyndFeedInput input  = new SyndFeedInput();
                    SyndFeed feed = input.build(new XmlReader(feedUrl));
                    List entries = feed.getEntries();

                    Iterator iterator = entries.listIterator();
                    while(iterator.hasNext()){
                        SyndEntry ent = (SyndEntry) iterator.next();

                        String title = ent.getTitle();
                        String link = ent.getLink();
                        String description = ent.getDescription().getValue();
                        Feed f = new Feed();
                        f.setId(++maxID);
                        f.setHeader(title);
                        f.setBody(description);
                        f.setLink(link);
                        createdFeeds.add(f);
                    }

                    validData = true;
                }catch(Exception e){
                    validData = false;
                    e.printStackTrace();
                }
            }
            Feed [] array = new Feed[createdFeeds.size()];
            createdFeeds.toArray(array);
            feedDao.deleteAll();
            feedDao.insert(array);

            feedData = feedDao.getAll();

            return validData;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            downloadStatus.setValue(Resource.LOADING_STATUS);
        }

        @Override
        protected void onPostExecute(Boolean success) {
            downloadStatus.setValue(success ? Resource.SUCCESS_STATUS : Resource.ERROR_STATUS);
        }
    }

}