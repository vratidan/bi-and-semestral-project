package cz.cvut.vratidan.room.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import cz.cvut.vratidan.FeedReaderApplication;
import cz.cvut.vratidan.room.dao.SourceDao;
import cz.cvut.vratidan.room.model.Source;
import cz.cvut.vratidan.room.repository.FeedRepository;

public class SourceViewModel extends AndroidViewModel {

    private SourceDao sourceDao;

    private FeedRepository repository;

    private LiveData<List<Source>> sourceData;


    public SourceViewModel(@NonNull Application application) {
        super(application);

        this.repository = FeedReaderApplication.get(application).getFeedRepository();

        this.sourceData = this.repository.getAllSources();
    }

    public void delete(Source source){ this.repository.deleteSource(source); }

    public int getMaxId(){ return this.repository.getSourceMaxId(); };

    public void insert(Source ...sources){ this.repository.insertSource(sources); };

    public LiveData<List<Source>> getSourceList(){
        return sourceData;
    }

    public Source findSourceById(int id){
        return this.repository.findSourceById(id);
    }
}
