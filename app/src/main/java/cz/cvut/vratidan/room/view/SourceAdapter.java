package cz.cvut.vratidan.room.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cz.cvut.vratidan.R;
import cz.cvut.vratidan.databinding.SourceItemBinding;
import cz.cvut.vratidan.room.model.Source;

public class SourceAdapter extends  RecyclerView.Adapter<SourceAdapter.SourceViewHolder>{

    List<? extends Source> mSourceList;

    @Nullable
    private final SourceAdapter.SourceClickedListener sourceClickedListener;

    public SourceAdapter(@Nullable SourceAdapter.SourceClickedListener clickCallback) {
        sourceClickedListener = clickCallback;
        setHasStableIds(true);
    }

    public void setSourceList(final List<? extends Source> sourceList) {
        if (mSourceList == null) {
            mSourceList = sourceList;
            notifyItemRangeInserted(0, sourceList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mSourceList.size();
                }

                @Override
                public int getNewListSize() {
                    return sourceList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mSourceList.get(oldItemPosition).getId() ==
                            sourceList.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Source newSource = sourceList.get(newItemPosition);
                    Source oldSource = mSourceList.get(oldItemPosition);
                    return newSource.getId() == oldSource.getId();
                }
            });
            mSourceList = sourceList;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public SourceAdapter.SourceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        SourceItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.source_item,
                        parent, false);
        return new SourceAdapter.SourceViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SourceAdapter.SourceViewHolder holder, int position) {
        final Source source = mSourceList.get(position);

        holder.binding.setSource(source);
        holder.binding.executePendingBindings();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sourceClickedListener != null) {
                    sourceClickedListener.onSourceClicked(source);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSourceList == null ? 0 : mSourceList.size();
    }

    @Override
    public long getItemId(int position) {
        return mSourceList.get(position).getId();
    }

    public Source getItem(int position) {
        return mSourceList.get(position);
    }

    static class SourceViewHolder extends RecyclerView.ViewHolder {

        final SourceItemBinding binding;

        public SourceViewHolder(SourceItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface SourceClickedListener {
        void onSourceClicked(Source source);
    }

}
