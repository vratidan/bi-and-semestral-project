package cz.cvut.vratidan.room;

public class Resource<T> {

    public static final int LOADING_STATUS = 0;
    public static final int ERROR_STATUS = 1;
    public static final int SUCCESS_STATUS = 2;
    public static final int IDLE_STATUS = 3;

    private int status;
    private T data;
    private String msg;

    public Resource(int status, T data, String msg) {
        this.status = status;
        this.data = data;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public String getMsg() {
        return msg;
    }

    public boolean isSuccess() {
        return status == SUCCESS_STATUS;
    }

    public boolean isError() {
        return status == ERROR_STATUS;
    }

    public boolean isLoading() {
        return status == LOADING_STATUS;
    }
}

