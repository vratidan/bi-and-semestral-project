package cz.cvut.vratidan.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cz.cvut.vratidan.room.model.Feed;

@Dao
public interface FeedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Feed... feeds);

    @Update
    void update(Feed... feeds);

    @Delete
    void delete(Feed feed);

    @Query("SELECT * FROM feed")
    LiveData<List<Feed>> getAll();

    @Query("SELECT * FROM feed WHERE id = :id")
    Feed findById(int id);

    @Query("SELECT MAX(feed.id) FROM feed")
    int getMaxId();

    @Query("DELETE FROM feed")
    void deleteAll();
}
