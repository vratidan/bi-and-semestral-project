package cz.cvut.vratidan.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import cz.cvut.vratidan.room.dao.FeedDao;
import cz.cvut.vratidan.room.dao.SourceDao;
import cz.cvut.vratidan.room.model.Feed;
import cz.cvut.vratidan.room.model.Source;


@Database(entities = {Feed.class, Source.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DB_NAME = "feed_db";
    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }


    public abstract FeedDao getFeedDao();
    public abstract SourceDao getSourceDao();
}
