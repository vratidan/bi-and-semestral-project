package cz.cvut.vratidan.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cz.cvut.vratidan.room.model.Source;

@Dao
public interface SourceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Source... sources);

    @Update
    void update(Source... sources);

    @Delete
    void delete(Source source);

    @Query("SELECT * FROM source")
    LiveData<List<Source>> getAll();

    @Query("SELECT * FROM source WHERE id = :id")
    Source findById(int id);

    @Query("SELECT MAX(source.id) FROM source")
    int getMaxId();

    @Query("DELETE FROM source")
    void deleteAll();
    
}
