package cz.cvut.vratidan.room.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cz.cvut.vratidan.R;
import cz.cvut.vratidan.databinding.FeedItemBinding;
import cz.cvut.vratidan.room.model.Feed;


public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.FeedViewHolder>{

    List<? extends Feed> mFeedList;

    @Nullable
    private final FeedClickedListener feedClickedListener;

    public FeedAdapter(@Nullable FeedClickedListener clickCallback) {
        feedClickedListener = clickCallback;
        setHasStableIds(true);
    }

    public void setFeedList(final List<? extends Feed> feedList) {
        if (mFeedList == null) {
            mFeedList = feedList;
            notifyDataSetChanged();
        } else {
            if(feedList == null){
                return;
            }
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mFeedList.size();
                }

                @Override
                public int getNewListSize() {
                    return feedList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mFeedList.get(oldItemPosition).getId() ==
                            feedList.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Feed newFeed = feedList.get(newItemPosition);
                    Feed oldFeed= mFeedList.get(oldItemPosition);
                    return newFeed.getId() == oldFeed.getId();
                }
            });
            mFeedList = feedList;
            notifyDataSetChanged();
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FeedItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.feed_item,
                        parent, false);
        return new FeedViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, int position) {
        final Feed feed = mFeedList.get(position);

        holder.binding.setFeed(feed);
        holder.binding.executePendingBindings();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(feedClickedListener != null) {
                    feedClickedListener.onFeedClicked(feed);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFeedList == null ? 0 : mFeedList.size();
    }

    @Override
    public long getItemId(int position) {
        return mFeedList.get(position).getId();
    }

    public Feed getItem(int position) {
        return mFeedList.get(position);
    }

    static class FeedViewHolder extends RecyclerView.ViewHolder {

        final FeedItemBinding binding;

        public FeedViewHolder(FeedItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface FeedClickedListener {
        void onFeedClicked(Feed feed);
    }
}
