package cz.cvut.vratidan;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import cz.cvut.vratidan.room.model.Source;

public class RemoveSourceDialogFragment extends DialogFragment {

    public SourceFragment sf;

    public Source source;

    public RemoveSourceDialogFragment(){}

    public static RemoveSourceDialogFragment newInstance(SourceFragment sf, Source source) {

        Bundle args = new Bundle();

        RemoveSourceDialogFragment fragment = new RemoveSourceDialogFragment();
        fragment.setArguments(args);
        fragment.sf = sf;
        fragment.source = source;

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = requireActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.remove_source_dialog, null))
                .setPositiveButton(getString(R.string.remove), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        removeSourceFromDatabase();
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RemoveSourceDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    public void removeSourceFromDatabase(){
        this.sf.sourceViewModel.delete(this.source);
    }

}
