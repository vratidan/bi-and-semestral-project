package cz.cvut.vratidan;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;

import java.util.List;

import cz.cvut.vratidan.room.Resource;
import cz.cvut.vratidan.room.model.Feed;

public class MainActivity extends AppCompatActivity implements ListFragment.ListFragmentListener, DetailFragment.DetailFragmentListener {

    public Menu menu;

    @Override
    public void feedClickedInListener(Feed feed) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            DetailFragment df = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.detail_fragment);
            if(df != null && feed != null)
                df.setFeed(feed);
        }
    }

    public int nextJobId;

    public JobScheduler jobScheduler;

    public int periodicJobId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.list_toolbar);

        toolbar.setTitle(getString(R.string.app_name));

        setSupportActionBar(toolbar);

        nextJobId = 1;

        jobScheduler = (JobScheduler)getApplicationContext()
                .getSystemService(JOB_SCHEDULER_SERVICE);

        List<JobInfo> jobList = jobScheduler.getAllPendingJobs();

        boolean periodicScheduled = false;

        for(JobInfo jobInfo
            :   jobList){
            if(jobInfo.getId() == periodicJobId)
                periodicScheduled = true;
        }

        if(!periodicScheduled)
            scheduleJob(true);

    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);

    }

    private void scheduleJob(boolean periodic){
        ComponentName componentName = new ComponentName(this,
                DownloadJobService.class);

        JobInfo jobInfo;

        if(periodic){
             jobInfo = new JobInfo.Builder(periodicJobId, componentName)
                .setPeriodic(1000 * 60 * 60 * 2) // 2 Hours
                .setPersisted(true)
                .build();
        }else{
            jobInfo = new JobInfo.Builder(++nextJobId, componentName)
                .setMinimumLatency(1)
                .setOverrideDeadline(1)
                .setPersisted(true)
                .build();
        }

        jobScheduler.schedule(jobInfo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        this.menu = menu;

        FragmentManager fm = getSupportFragmentManager();
        ListFragment list_fragment = (ListFragment) fm.findFragmentById(R.id.list_fragment);
        if(list_fragment != null && list_fragment.feedViewModel != null)
            list_fragment.feedViewModel.repository.downloadStatus.observeForever(new Observer<Integer>() {
                @Override
                public void onChanged(Integer integer) {
                    if(integer != Resource.LOADING_STATUS)
                        menu.findItem(R.id.reload_action).setActionView(null);
                    else
                        menu.findItem(R.id.reload_action).setActionView(new ProgressBar(getApplicationContext()));
                }
            });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.configure_feeds){
            Intent intent = new Intent(this, SourceActivity.class);
            this.startActivity(intent);
        }

        if(id == R.id.reload_action){

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                DetailFragment detailFragment = DetailFragment.newInstance();
                detailFragment.setArguments(new Bundle());

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.detail_fragment, detailFragment)
                        .commit();
            }

            item.setActionView(new ProgressBar(getApplicationContext()));

            scheduleJob(false);
        }

        return super.onOptionsItemSelected(item);
    }

}
