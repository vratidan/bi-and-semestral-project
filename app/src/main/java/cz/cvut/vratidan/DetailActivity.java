package cz.cvut.vratidan;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

public class DetailActivity extends AppCompatActivity implements DetailFragment.DetailFragmentListener {

    public Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);

        toolbar.setTitle(R.string.detail_activity);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if(savedInstanceState == null || getSupportFragmentManager().getFragment(savedInstanceState, getString(R.string.detail_fragment_name)) == null) {
            String detail_fragment_name = getString(R.string.detail_fragment_name);

            DetailFragment df =  DetailFragment.newInstance();
            Bundle b = new Bundle();
            b.putInt(getString(R.string.feed_id_name), getIntent().getIntExtra(getString(R.string.feed_id_name), -1));
            df.setArguments(b);



            getSupportFragmentManager().beginTransaction()
                    .add(R.id.detail_fragment, df)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.share_action){

            FragmentManager fm = getSupportFragmentManager();
            DetailFragment detailFragment = (DetailFragment) fm.findFragmentById(R.id.detail_fragment);

            Intent i = new Intent();
            i.setAction(Intent.ACTION_SEND);
            i.putExtra(Intent.EXTRA_SUBJECT, detailFragment.getFeed().getHeader() == null ? "" : detailFragment.getFeed().getHeader());
            i.putExtra(Intent.EXTRA_TEXT, detailFragment.getFeed().getBody() == null ? "" : detailFragment.getFeed().getBody());
            i.setType("text/plain");

            Intent shareIntent = Intent.createChooser(i, getString(R.string.choose_application));
            startActivity(shareIntent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
