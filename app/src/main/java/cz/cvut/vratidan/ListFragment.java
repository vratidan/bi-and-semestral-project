package cz.cvut.vratidan;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import cz.cvut.vratidan.databinding.FeedListBinding;
import cz.cvut.vratidan.room.Resource;
import cz.cvut.vratidan.room.model.Feed;
import cz.cvut.vratidan.room.view.FeedAdapter;
import cz.cvut.vratidan.room.viewmodel.FeedViewModel;

public class ListFragment extends Fragment {

    public ListFragmentListener listener;

    public FeedViewModel feedViewModel;

    public FeedListBinding feedListBinding;

    public FeedAdapter feedAdapter;

    @NonNull
    public static ListFragment newInstance(){
        return new ListFragment();
    }

    public ListFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.feedViewModel = ViewModelProviders.of(this).get(FeedViewModel.class);
    }

    public void feedClicked(Feed feed){
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            this.listener.feedClickedInListener(feed);
        }else{
            Intent myIntent = new Intent(getActivity(), DetailActivity.class);
            myIntent.putExtra(getString(R.string.feed_id_name), feed.getId());
            getActivity().startActivity(myIntent);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.feedListBinding = DataBindingUtil.inflate(inflater, R.layout.feed_list, container, false);

        this.feedListBinding.setListFragment(this);

        this.feedAdapter = new FeedAdapter(new FeedAdapter.FeedClickedListener() {
            @Override
            public void onFeedClicked(Feed feed) {
                feedClicked(feed);
            }
        });

        this.feedListBinding.feedList.setAdapter(this.feedAdapter);

        this.feedListBinding.setViewmodel(this.feedViewModel);

        this.feedViewModel.getFeeds().observeForever(new Observer<List<Feed>>() {
            @Override
            public void onChanged(List<Feed> feeds) {
                if(feeds != null && !feeds.isEmpty()){
                    feedAdapter.setFeedList(feeds);
                }
                feedListBinding.setFeedResource(new Resource(feedViewModel.getDownloadStatus().getValue(), feeds, null));
                feedListBinding.executePendingBindings();
            }
        });

        this.feedViewModel.getDownloadStatus().observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(Integer status) {
                feedListBinding.setFeedResource(new Resource(status, feedViewModel.getFeeds().getValue(), null));
                feedListBinding.executePendingBindings();
            }
        });

        return this.feedListBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            this.listener = (ListFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement ListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    public interface ListFragmentListener {
        void feedClickedInListener(Feed feed);
    }

}
