package cz.cvut.vratidan;

import android.app.Application;
import android.content.Context;

import cz.cvut.vratidan.room.AppDatabase;
import cz.cvut.vratidan.room.repository.FeedRepository;

public class FeedReaderApplication extends Application {

    private FeedRepository feedRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        initRoom();
    }

    public static FeedReaderApplication get(Context context){
        return (FeedReaderApplication) context.getApplicationContext();
    }

    private void initRoom(){
        AppDatabase appDatabase = AppDatabase.getInstance(this);
        feedRepository = new FeedRepository(appDatabase.getFeedDao(), appDatabase.getSourceDao());
    }

    public FeedRepository getFeedRepository() {
        return feedRepository;
    }
}
