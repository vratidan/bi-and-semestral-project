package cz.cvut.vratidan;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import cz.cvut.vratidan.databinding.SourceListBinding;
import cz.cvut.vratidan.room.Resource;
import cz.cvut.vratidan.room.model.Source;
import cz.cvut.vratidan.room.view.SourceAdapter;
import cz.cvut.vratidan.room.viewmodel.SourceViewModel;

public class SourceFragment extends Fragment {

    public SourceFragmentListener listener;

    private SourceListBinding sourceListBinding;

    public SourceViewModel sourceViewModel;

    private SourceAdapter sourceAdapter;

    @NonNull
    public static SourceFragment newInstance(){
        return new SourceFragment();
    }

    public SourceFragment() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.sourceListBinding = DataBindingUtil.inflate(inflater, R.layout.source_list, container, false);

        this.sourceListBinding.setSourceFragment(this);

        this.sourceAdapter = new SourceAdapter(new SourceAdapter.SourceClickedListener() {
            @Override
            public void onSourceClicked(Source source) {
                sourceClicked(source);
            }
        });

        this.sourceListBinding.sourceList.setAdapter(this.sourceAdapter);

        this.sourceViewModel = ViewModelProviders.of(this).get(SourceViewModel.class);

        this.sourceListBinding.setViewmodel(this.sourceViewModel);

        this.sourceViewModel.getSourceList().observeForever(new Observer<List<Source>>() {
            @Override
            public void onChanged(List<Source> sources) {
                if(sources != null && !sources.isEmpty()){
                    sourceAdapter.setSourceList(sources);
                    sourceListBinding.setSourceResource(new Resource(Resource.SUCCESS_STATUS, sources, null));
                }
                sourceListBinding.executePendingBindings();
            }
        });
        return this.sourceListBinding.getRoot();
    }

    public void sourceClicked(Source source){
        FragmentManager fm = getActivity().getSupportFragmentManager();
        RemoveSourceDialogFragment dialog = RemoveSourceDialogFragment.newInstance(this, source);
        dialog.show(fm, getString(R.string.add_source_dialog_tag));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            this.listener = (SourceFragment.SourceFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement SourceFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    public interface SourceFragmentListener {

    }
}
