package cz.cvut.vratidan;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import cz.cvut.vratidan.databinding.FeedDetailBinding;
import cz.cvut.vratidan.room.model.Feed;
import cz.cvut.vratidan.room.viewmodel.FeedViewModel;

public class DetailFragment extends Fragment {

    public DetailFragmentListener listener;

    public FeedDetailBinding feedDetailBinding;

    private FeedViewModel feedViewModel;

    private int id = -1;

    private Feed feed;

    @NonNull
    public static DetailFragment newInstance(){
        return new DetailFragment();
    }

    public DetailFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.feedDetailBinding = DataBindingUtil.inflate(inflater, R.layout.feed_detail, container, false);

        this.feedViewModel = ViewModelProviders.of(this).get(FeedViewModel.class);

        View root = this.feedDetailBinding.getRoot();

        Bundle b = getArguments();

        if(b != null && b.containsKey(getString(R.string.feed_id_name))){
            this.id = b.getInt(getString(R.string.feed_id_name));
        }
        else if(savedInstanceState != null && savedInstanceState.containsKey(getString(R.string.feed_id_name))){
            this.id = savedInstanceState.getInt(getString(R.string.feed_id_name));
        }

        if(this.id != -1){
            this.feed = this.feedViewModel.findById(this.id);
            this.feedDetailBinding.setVisible(true);
            this.feedDetailBinding.setFeed(this.feed);
            TextView detail_link = (TextView) root.findViewById(R.id.detail_link);
            detail_link.setOnClickListener(new DetailLinkListener(this.feed.getLink()));
        }
        else{
            this.feedDetailBinding.setVisible(false);
            this.feedDetailBinding.setFeed(null);
        }

        this.feedDetailBinding.executePendingBindings();

        this.setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
                this.listener = (DetailFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement DetailFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed){
        this.feed = feed;
        this.feedDetailBinding.setVisible(true);
        this.feedDetailBinding.setFeed(feed);
        TextView detail_link = (TextView) this.feedDetailBinding.getRoot().findViewById(R.id.detail_link);
        detail_link.setOnClickListener(new DetailLinkListener(this.feed.getLink()));
        this.feedDetailBinding.executePendingBindings();
    }

    public class DetailLinkListener implements View.OnClickListener{

        public String link;

        public DetailLinkListener(String link) {
            this.link = link;
        }

        @Override
        public void onClick(View view){
            if(this.link == null || this.link.equals(""))
                return;
            Uri uri = Uri.parse(this.link);
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(browserIntent);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if(this.feed != null)
            outState.putInt(getString(R.string.feed_id_name), this.feed.getId());
    }

    public interface DetailFragmentListener { }

}
